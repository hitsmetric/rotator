<?php

/**
 * Cron file
 * Class for verifing counts execs processes
 *
 *
 * Examples 
    $cronfile = new Cronfile(__FILE__);
    $chp = $cronfile->CheckCountProcesses(1);
    if(!$chp){
        die('Too many processes');
    }
    $chpid = $cronfile->CheckCountProcessByPid($pid,1);
    if(!$chpid){
        die('Too many processes by pid');
    }
 * 
 */
class CronFile{
	/**
	 *  Path to file
	 */
	private $file;
	
	/**
	 * Count worked processes by pid
	 *
	 * @var unknown_type
	 */
	private $count_process_by_pid;
	
	/**
	 * Script name
	 *
	 * @var unknown_type
	 */
	private $name;
	
	 /**
	  * Count worked processes
	  *
	  * @var unknown_type
	  */
	private $count_process;

	 /**
	  * Pid processes
	  *
	  * @var unknown_type
	  */
	private $pid;
	
	 /**
	  * Constructor
	  *
	  * @param unknown_type $file
	  * @return CronFile
	  */
	public function CronFile($file){
		$this->file = $file;		
		$this->name = basename($this->file);				
	}
	
	/**
	 * Check count processes
	 *
	 * @param int $count
	 * @return bool
	 */
	public function CheckCountProcesses($count = 1) {
		$name = $this->name;
//		$query = "ps -o ppid -o tid  -o cmd -C 'php' | grep -c '$name'";
        $query = "ps -o cmd -C 'php' | grep -c '$name'";
		$this->execProcess($query, $return , $error);
		if ((int)trim($return) > $count || $error!=='') {
			return false;
		}
		return true;
	}
	
	/**
	 * Check count processes by pid	
	 * 
	 * @param $count
	 * @return unknown
	 */
	public function CheckCountProcessByPid($pid, $count = 1) {
		$query = "ps -o ppid -o tid  -o cmd -C 'php' | grep -c '$this->name $pid'";
		$return = $error = null;
		$this->execProcess($query, $return , $error);		
		if($return > $count || $error!==''){
			return false;
		}
		return true;
	}
	
	
	
	/**
	 * Exec process
	 *
	 * @param string $cmd
	 * @param string $return
	 * @param string $error
	 */
	private function execProcess ($cmd, &$return, &$error) {
		$descriptorspec = array(
		   0 => array("pipe", "r"),  // stdin 
		   1 => array("pipe", "w"),  // stdout 
		   2 => array("pipe", "w")   // stderr
		);
		$cwd = '/tmp';
		$env = array();
		$pipes = '';	
		$process = proc_open($cmd, $descriptorspec, $pipes, $cwd, $env);	
		if (is_resource($process)) {
			//read stdout 
		    $return = trim(stream_get_contents($pipes[1]));
		    fclose($pipes[1]);
			//read stderr 
		    $error = trim(stream_get_contents($pipes[2]));
			fclose($pipes[2]);
			$return_value = proc_close($process);		    
		}
		return $return_value;
	}
}

?>
